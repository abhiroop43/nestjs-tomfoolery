export class AddProductDto {
  productName: string;
  description: string;
  price: number;
}
