import { Injectable, NotFoundException } from '@nestjs/common';
import { AddProductDto } from './add-product-dto';
import { ProductModel } from './product-model';
import { UpdateProductDto } from './update-product-dto';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

@Injectable()
export class ProductService {

  constructor(
    @InjectModel('Product') private readonly productModel: Model<ProductModel>,
  ) {}

  async addNewProduct(product: AddProductDto): Promise<string> {
    const prod = new this.productModel({
      productName: product.productName,
      description: product.description,
      price: product.price,
    });

    const result = await prod.save();
    // console.log(result);
    return result.id as string;
  }

  async updateProduct(
    productId: string,
    updatedProduct: UpdateProductDto,
  ): Promise<string> {
    const foundProduct = await this.findProduct(productId);

    if (updatedProduct.description)
      foundProduct.description = updatedProduct.description;

    if (updatedProduct.productName)
      foundProduct.productName = updatedProduct.productName;

    if (updatedProduct.price) foundProduct.price = updatedProduct.price;

    await foundProduct.save();

    return foundProduct.id;
  }

  async getAllProducts(): Promise<any> {
    const products = await this.productModel.find().exec();
    return products.map((prod) => ({
      id: prod.id,
      productName: prod.productName,
      description: prod.description,
      price: prod.price,
    }));
  }

  async getProductDetails(id: string): Promise<any> {
    const foundProduct = await this.findProduct(id);
    return {
      id: foundProduct.id,
      description: foundProduct.description,
      productName: foundProduct.productName,
      price: foundProduct.price,
    };
  }

  async deleteProduct(prodId: string): Promise<any> {
    const result = await this.productModel.deleteOne({ _id: prodId }).exec();
    if (result.n === 0) {
      throw new NotFoundException('Product not found');
    }

    return prodId;
  }

  private async findProduct(id: string): Promise<ProductModel> {
    let foundProduct;

    try {
      foundProduct = await this.productModel.findById(id);
    } catch (error) {
      throw new NotFoundException('Product not found');
    }

    if (!foundProduct) {
      throw new NotFoundException('Product not found');
    }

    return foundProduct;
  }
}
