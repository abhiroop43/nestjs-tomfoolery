import { AddProductDto } from './add-product-dto';

describe('AddProductDto', () => {
  it('should be defined', () => {
    expect(new AddProductDto()).toBeDefined();
  });
});
