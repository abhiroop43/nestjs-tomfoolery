export class UpdateProductDto {
  productName?: string;
  description?: string;
  price?: number;
}
