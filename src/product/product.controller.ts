import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Patch,
  Delete,
} from '@nestjs/common';
import { ProductService } from './product.service';
import { AddProductDto } from './add-product-dto';
import { ProductModel } from './product-model';
import { UpdateProductDto } from './update-product-dto';

@Controller('product')
export class ProductController {
  constructor(private readonly productService: ProductService) {}

  @Get('getAllProducts')
  async getAllProducts(): Promise<any> {
    const products = await this.productService.getAllProducts();
    return products;
  }

  @Get('getProduct/:id')
  async getProduct(@Param('id') id: string): Promise<any> {
    return await this.productService.getProductDetails(id);
  }

  @Post('addProduct')
  async addNewProduct(@Body() newProduct: AddProductDto): Promise<any> {
    const generatedId = await this.productService.addNewProduct(newProduct);
    return { id: generatedId };
  }

  @Patch('updateProduct/:id')
  async updateProduct(
    @Param('id') id: string,
    @Body() updateProduct: UpdateProductDto,
  ): Promise<any> {
    const updatedProductId = await this.productService.updateProduct(
      id,
      updateProduct,
    );
    return { id: updatedProductId };
  }

  @Delete('deleteProduct/:id')
  async deleteProduct(@Param('id') id: string): Promise<any> {
    return await this.productService.deleteProduct(id);
  }
}
