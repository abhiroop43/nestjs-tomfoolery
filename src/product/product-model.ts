import * as mongoose from 'mongoose';

export const ProductSchema = new mongoose.Schema({
  productName: { type: String, required: true },
  description: { type: String, required: true },
  price: { type: Number, required: true },
});

export interface ProductModel extends mongoose.Document {
  id: string;
  productName: string;
  description: string;
  price: number;
}
